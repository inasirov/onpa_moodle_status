﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Configuration;
using System.Net.Mail;
using System.Runtime.Caching;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.ServiceProcess;
using System.Text;
using System.Timers;
using System.Web.Script.Serialization;

namespace ONPHA_Moddle_Status_Test
{
    public partial class Scheduler: ServiceBase
    {

        #region Consts
        
        public string webserviceurl = ConfigurationManager.AppSettings["WebServiceUrl"].ToString(); // "http://moodlesandbox10.lambdasolutions.net/webservice/rest/server.php";
        public string moodleCourses = ConfigurationManager.AppSettings["moodleCourses"].ToString(); // "http://moodlesandbox10.lambdasolutions.net/course/view.php";
        public string token = ConfigurationManager.AppSettings["token"].ToString(); //"f4fbec2886f30f4c063b40f689aad268";
        public double scheduledHour = double.Parse(ConfigurationManager.AppSettings["scheduled_hour"]);
        public double emailHour = double.Parse(ConfigurationManager.AppSettings["email_hour"]);
        public string emailAddresses = ConfigurationManager.AppSettings["email_address"].ToString();
        public DateTime scheduledTime;
        public DateTime emailTime;
        #endregion

        #region Constructors
        public Scheduler()
        {
            try
            {
                InitializeComponent();
                scheduledTime = DateTime.Today.AddHours(scheduledHour);
                emailTime = DateTime.Today.AddHours(emailHour);
            }
            catch(Exception ex)
            {
                Library.WriteErrorLog("Error, Scheduler(), ex.message: " + ex.Message);
            }
            
        }
        #endregion

        #region Variables

        /// <summary>
        /// Wake up timer
        /// <remarks>Uses to chesk if it is time to redo synchronization</remarks>
        /// </summary>
        private Timer _wakeUpTimer;
        /// <summary>
        /// Timer check interval in ms
        /// </summary>
        private double _checkInterval;
        /// <summary>
        /// Time when synchronization should be redone
        /// </summary>
        private DateTime _regenerateTime;

        private bool _runImmediately;

        protected List<ReportedError> ErrorsList;
        #endregion

        #region Base Methods

        protected override void OnStart(string[] args)
        {
            StartMonitoring();
        }

        protected override void OnStop()
        {
            StopMonitoring();
        }

        /// <summary>
        /// Pause service
        /// </summary>
        protected override void OnPause()
        {
            StopMonitoring();
        }

        /// <summary>
        /// Continue service
        /// </summary>
        protected override void OnContinue()
        {
            ContinueMonitoring();
        }

        /// <summary>
        /// Start monitoring process
        /// </summary>
        private void StartMonitoring()
        {
            Library.WriteErrorLog("Service started");
            SetNextGenerateTime();
            _wakeUpTimer = new Timer
            {
                Enabled = false
            };
            _runImmediately = bool.Parse(System.Configuration.ConfigurationManager.AppSettings["RunImmediately"].ToString());
            // Get interval from configuration
            int intcheck = int.Parse(System.Configuration.ConfigurationManager.AppSettings["TimeIntervalCheck"].ToString());
            if (intcheck > 0)
            {
                _checkInterval = intcheck * 1000; // parameter from configuration is set in seconds
            }
            else
            {
                _checkInterval = 1000; //every 1 second
            }
            _wakeUpTimer.Interval = _checkInterval;
            _wakeUpTimer.Elapsed += WakeUpTimerElapsed;

            _wakeUpTimer.Enabled = true;
            _wakeUpTimer.Start();
        }

        /// <summary>
        /// Stop monitoring process
        /// </summary>
        private void StopMonitoring()
        {
            if (_wakeUpTimer.Enabled)
            {
                _wakeUpTimer.Enabled = false;
                _wakeUpTimer.Stop();
                Library.WriteErrorLog("Service stopped");
            }
        }

        /// <summary>
        /// Continue monitoring process
        /// </summary>
        private void ContinueMonitoring()
        {
            SetNextGenerateTime();

            _wakeUpTimer.Enabled = true;
            _wakeUpTimer.Start();
            //SaveErrorLog("ServiceInfo", string.Format("{0} - {1}", DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss"), "VA Role Synchronization Service continued"));
            Library.WriteErrorLog("Service continued");
        }

        private void SetNextGenerateTime()
        {
            int seconds = int.Parse(System.Configuration.ConfigurationManager.AppSettings["TimeIntervalRun"].ToString());
            var now = DateTime.Now;
            _regenerateTime = now.AddSeconds(seconds);
        }

        /// <summary>
        /// Timer tick event
        /// </summary>
        /// <param name="sender">Timer</param>
        /// <param name="e">Timer event arguments</param>
        private void WakeUpTimerElapsed(object sender, ElapsedEventArgs e)
        {
            // Check if need to synchronize
            if (e.SignalTime >= _regenerateTime || _runImmediately)
            {
                StartSynchroniazation();
                _runImmediately = false;
            }
        }

        /// <summary>
        /// Start cpd synchronization
        /// </summary>
        private void StartSynchroniazation()
        {
            try
            {
                StopMonitoring();
                // Main method
                //MainService();
                MainServiceNew();
                // end
            }
            catch (Exception ex)
            {
                Library.WriteErrorLog(ex);
                var newError = new ReportedError();
                newError.ErrorType = ReportErrorType.WindowsServiceError;
                newError.ErrorMessage = "Web System General Error: " + ex.Message;
                newError.MemberID = "39796";  // manager's ID
                newError.MeetCode = string.Empty;
                newError.MoodleEventId = 0;
                AddReportedMessage(newError);
            }
            finally
            {
                ContinueMonitoring();
            }
        }

        protected void MainService()
        {
            Library.WriteErrorLog("MainService - Begin");

            Library.WriteErrorLog(string.Format("MainService, Scheduled Time :{0}", scheduledTime.ToString("yyyy-MM-dd hh:mm:ss")));
            bool checkCourseResults = false;
            if (scheduledTime < DateTime.Now)
            {
                checkCourseResults = true;
                scheduledTime = scheduledTime.AddDays(1);
            }

            var list = GetAllEvents();
            Library.WriteErrorLog(string.Format("MainService, List.Count :{0}", (list != null ? list.Count().ToString() : "List = null")));

            var mList = getMoodleAllCourses();
            Library.WriteErrorLog(string.Format("MainService, mList.Count :{0}", (mList != null && mList.ResultSuccess != null ? mList.ResultSuccess.Count().ToString(): "mList = null")));

            if (mList != null)
            {
                if (mList.ResultSuccess != null)
                {
                    string coursesList = string.Empty;
                    var fList = mList.ResultSuccess.Where(I => I.visible == 1).ToList();
                    foreach (var item in fList)
                    {
                        var findEvent = list.Where(I => I.MeetingCode == item.idnumber).FirstOrDefault();
                        if (findEvent != null)
                        {
                            findEvent.moodleEvent = true; list.Find(I => I.MeetingCode == findEvent.MeetingCode).moodleEvent = true;
                            list.Find(I => I.MeetingCode == findEvent.MeetingCode).moodleEventId = item.id;
                        }
                    }

                    foreach (var item1 in list)
                    {
                        if (item1.moodleEvent) //  && item1.moodleEventId == 22
                        {
                            CheckEvent(item1.MeetingCode, item1.moodleEventId, checkCourseResults);
                            Library.WriteErrorLog(string.Format("MainService, Event - Code :{0}, Title: {1}, Moodle Id: {2} ", item1.MeetingCode, item1.MeetingTitle, item1.moodleEventId));
                        }
                    }
                }
                else if (mList.ResultError != null)
                {
                    Library.WriteErrorLog(" Error - MainService().getMoodleAllCourses().ResultError: " + mList.ResultError.message, true);
                    var newError = new ReportedError();
                    newError.ErrorType = ReportErrorType.MoodleServiceError;
                    newError.ErrorMessage = "Moodle System Error on getting list of courses: " + mList.ResultError.message;
                    newError.MemberID = "39796";  // manager's ID
                    newError.MeetCode = string.Empty;
                    newError.MoodleEventId = 0;
                    AddReportedMessage(newError);
                }
            }
            bool sendEmail = true;
            if (emailTime < DateTime.Now)
            {
                TimeSpan span = DateTime.Now.Subtract(emailTime);
                if (span.TotalMinutes < 10)
                {
                    sendEmail = true;
                }
                emailTime = emailTime.AddDays(1);
            }
            if (sendEmail) SendEmail();
            Library.WriteErrorLog(string.Format("MainService - emailTime: {0}", emailTime.ToString("yyyy-MM-dd HH:mm:ss")));
            Library.WriteErrorLog("MainService - End");
        }

        protected void MainServiceNew()
        {
            Library.WriteErrorLog("0. MainService - Begin");

            Library.WriteErrorLog(string.Format("1. MainService, Scheduled Time :{0}", scheduledTime.ToString("yyyy-MM-dd hh:mm:ss")));
            bool checkCourseResults = false;
            if (scheduledTime < DateTime.Now)
            {
                checkCourseResults = true;
                scheduledTime = scheduledTime.AddDays(1);
            }

            var list = GetAllEvents();
            Library.WriteErrorLog(string.Format("2a. MainService, list.Count :{0}", (list != null ? list.Count().ToString() : "list = null")));

            var mList = getMoodleAllCourses();
            Library.WriteErrorLog(string.Format("2. MainService, mList.Count :{0}", (mList != null && mList.ResultSuccess != null ? mList.ResultSuccess.Count().ToString() : "mList = null")));

            if (mList != null)
            {
                if (mList.ResultSuccess != null)
                {
                    string coursesList = string.Empty;
                    var fList = mList.ResultSuccess.Where(I => I.visible == 1 && !string.IsNullOrEmpty(I.idnumber)).ToList();

                    Library.WriteErrorLog(string.Format("3. MainService, fList.Count :{0}", (fList != null ? fList.Count().ToString() : "fList = null")));
                    foreach (var item in fList)
                    {
                        Library.WriteErrorLog(string.Format("4. MainService, item.idnumber :{0}", item.idnumber));
                        var findEvent = list.Where(I => I.MeetingCode == item.idnumber).FirstOrDefault();
                        if (findEvent != null)
                        {
                            Library.WriteErrorLog(string.Format("5. MainService, findEvent.MeetingCode :{0}", findEvent.MeetingCode));
                            findEvent.moodleEvent = true; list.Find(I => I.MeetingCode == findEvent.MeetingCode).moodleEvent = true;
                            list.Find(I => I.MeetingCode == findEvent.MeetingCode).moodleEventId = item.id;
                        }
                    }

                    foreach (var item1 in list)
                    {
                        if (item1.moodleEvent) //  && item1.moodleEventId == 22
                        {
                            Library.WriteErrorLog(string.Format("6. MainService, Event - Code :{0}, Title: {1}, Moodle Id: {2} ", item1.MeetingCode, item1.MeetingTitle, item1.moodleEventId));
                            CheckEvent(item1.MeetingCode, item1.moodleEventId, checkCourseResults);
                            
                        }
                    }
                }
                else if (mList.ResultError != null)
                {
                    Library.WriteErrorLog(" Error - MainService().getMoodleAllCourses().ResultError: " + mList.ResultError.message, true);
                    var newError = new ReportedError();
                    newError.ErrorType = ReportErrorType.MoodleServiceError;
                    newError.ErrorMessage = "Moodle System Error on getting list of courses: " + mList.ResultError.message;
                    newError.MemberID = "39796";  // manager's ID
                    newError.MeetCode = string.Empty;
                    newError.MoodleEventId = 0;
                    AddReportedMessage(newError);
                }
            }
            bool sendEmail = false;
            if (emailTime < DateTime.Now)
            {
                TimeSpan span = DateTime.Now.Subtract(emailTime);
                if (span.TotalMinutes < 10)
                {
                    sendEmail = true;
                }
                emailTime = emailTime.AddDays(1);
            }
            if (sendEmail) SendEmail();
            Library.WriteErrorLog(string.Format("MainService - emailTime: {0}", emailTime.ToString("yyyy-MM-dd HH:mm:ss")));
            Library.WriteErrorLog("MainService - End");
        }

        protected void CheckEvent(string eventCode, int mEventID, bool checkCourseResults)
        {
            
            var orders = GetOrders(eventCode);
            Library.WriteErrorLog(string.Format("7. CheckEvent(), GetOrders - Code :{0}, Orders.Count: {1}, - Step 1 ", Escape(eventCode), orders.Count));
            foreach (var order in orders)
            {
                var paidOrder = GetPaidOrder(eventCode, order.ID);
                if (paidOrder!=null)
                {
                    Library.WriteErrorLog(string.Format("8. CheckEvent() - paidOrder, Event - Code :{0}, Event Id: {1}, User : {2}, Order_Num: {3} - Step 2 ", Escape(eventCode), Escape(mEventID.ToString()), Escape(order.ID), order.OrderNum));

                    var newContacts = getMoodleContact(order.ID, "idnumber");
                    if (newContacts != null)
                    {
                        Library.WriteErrorLog(string.Format("9. CheckEvent() - getMoodleContact, newContacts.Count: {0} - Step 3 ", newContacts.Count));
                    }
                    else
                    {
                        Library.WriteErrorLog(string.Format("9. CheckEvent() - getMoodleContact, newContacts.Count: 0 - Step 3"));
                    }

                    bool enrolled = false;
                    bool completed = false;
                    if (newContacts != null && newContacts.Count > 0)
                    {
                        var newContact = newContacts.FirstOrDefault();
                        Library.WriteErrorLog(string.Format("10. CheckEvent() - newContact exist in Moodle - Code :{0}, User : {1}, moodle Contact ID: {2} - Step 4 ", Escape(eventCode), Escape(order.ID), Escape(newContact.id.ToString())));

                        // checking if user enrolled to the course

                        
                        var registration = CheckMoodleCourseRegistrationInImis(order.ID, mEventID);
                        if (registration!= null)
                        {
                            enrolled = true;
                            if (registration.Completed) completed = true;
                        }
                        else
                        {
                            // enrollment to course if not done yet
                            var coursesResult0 = getMoodleCourses(newContact.id.ToString());
                            if (coursesResult0 != null)
                            {
                                //string coursesList = string.Empty;
                                if (coursesResult0.ResultSuccess != null)
                                {
                                    foreach (var course in coursesResult0.ResultSuccess)
                                    {
                                        //coursesList += string.Format("ID: {0}, iMisID: {1}, Title: {2}\n", course.id, course.idnumber, course.fullname);
                                        if (course.idnumber == eventCode)
                                        {
                                            enrolled = true;
                                            // adding this course registration to Moodle_CourseRegistrations table
                                            CreateMoodleCoourseRegistrationInImis(order.ID, eventCode, mEventID);
                                            Library.WriteErrorLog(string.Format("11c. CheckEvent() - CreateMoodleCoourseRegistrationInImis - EventCode :{0}, User ID : {1}, Moodle Event ID: {2} - Step 5, User is enrolled to the course, adding his record to MoodleRegistrations ", Escape(eventCode), Escape(order.ID), mEventID));
                                        }
                                    }
                                }
                                if (coursesResult0.ResultError != null)
                                {
                                    var newError = new ReportedError();
                                    newError.ErrorType = ReportErrorType.MoodleServiceError;
                                    newError.ErrorMessage = "11d. CheckEvent() - ERROR - Moodle System Error on getting list of member's courses: " + coursesResult0.ResultError.message;
                                    newError.MemberID = order.ID;
                                    newError.MeetCode = eventCode;
                                    newError.MoodleEventId = mEventID;
                                    AddReportedMessage(newError);
                                }
                            }
                        }


                        if (!enrolled)
                        {
                            var functionResult = getMoodleUserEnroledForCourse(newContact.id.ToString(), mEventID.ToString());
                            if (functionResult != null)
                            {
                                if (functionResult.ResultSuccess != null)
                                {
                                    Library.WriteErrorLog(string.Format("12b. Event - Code :{0}, User : {1}, moodle Contact ID: {2} - Step 6, User now get enrolled to the course ", Escape(eventCode), Escape(order.ID), Escape(newContact.id.ToString())));
                                    enrolled = true;
                                }
                                else if (functionResult.ResultError != null)
                                {
                                    Library.WriteErrorLog(string.Format("12c. Event - Code :{0}, User : {1}, moodle Contact ID: {2}, Error: {3} - Step 6, User didn't get enrolled to the course because of an error ", Escape(eventCode), Escape(order.ID), Escape(newContact.id.ToString()), functionResult.ResultError.message), true);
                                    var newError = new ReportedError();
                                    newError.ErrorType = ReportErrorType.MoodleServiceError;
                                    newError.ErrorMessage = "Moodle System Error on enrolling member to the course: " + functionResult.ResultError.message;
                                    newError.MemberID = order.ID;
                                    newError.MeetCode = eventCode;
                                    newError.MoodleEventId = mEventID;
                                    AddReportedMessage(newError);
                                }
                            }
                        }

                        if (!completed && checkCourseResults)
                        {
                            // cheecking  course complition
                            Library.WriteErrorLog(string.Format("13. getMoodleCourseResult, Event - Code :{0}, User : {1}, moodle Contact ID: {2} - Step 7", Escape(eventCode), Escape(order.ID), Escape(newContact.id.ToString())));
                            var coursesResult = getMoodleCourseResult(newContact.id.ToString(), mEventID.ToString());
                            if (coursesResult != null)
                            {
                                Library.WriteErrorLog(string.Format("13b. Event - Code :{0}, User : {1}, moodle Contact ID: {2}, coursesResult is not null - Step 8 ", Escape(eventCode), Escape(order.ID), Escape(newContact.id.ToString())));
                                if (coursesResult.ResultSuccess != null)
                                {
                                    Library.WriteErrorLog(string.Format("13c. Event - Code :{0}, User : {1}, moodle Contact ID: {2}, coursesResult.ResultSuccess is not null - Step 9 ", Escape(eventCode), Escape(order.ID), Escape(newContact.id.ToString())));
                                    if (coursesResult.ResultSuccess.completionstatus != null)
                                    {
                                        Library.WriteErrorLog(string.Format("13d. Event - Code :{0}, User : {1}, moodle Contact ID: {2}, coursesResult.ResultSuccess.completionstatus is not null - Step 10 ", Escape(eventCode), Escape(order.ID), Escape(newContact.id.ToString())));
                                        if (coursesResult.ResultSuccess.completionstatus.completed)
                                        {
                                            AddCompetionStatus(eventCode, order.ID, order.OrderDate, order.OrderNum);
                                            MarkCourseCompleted(order.ID, eventCode );
                                            Library.WriteErrorLog(string.Format("13e. Event - Code :{0}, User : {1}, Course Completed, new activity 'Passed' created, marked too ", Escape(eventCode), Escape(order.ID)));
                                            continue;
                                        }
                                    }
                                }
                                else if (coursesResult.ResultError != null)
                                {
                                    Library.WriteErrorLog(string.Format(" Error - MainService().CheckEvent().getMoodleCourseResult().ResultError: Id='{0}', Event='{1}', Exception.exception='{2}', Exception.message='{3}', Exception.errorcode='{4}'", order.ID, eventCode, coursesResult.ResultError.exception, coursesResult.ResultError.message, coursesResult.ResultError.errorcode), true);
                                    var newError = new ReportedError();
                                    newError.ErrorType = ReportErrorType.MoodleServiceError;
                                    newError.ErrorMessage = "Moodle System Error on getting course results for member: " + coursesResult.ResultError.message;
                                    newError.MemberID = order.ID;
                                    newError.MeetCode = eventCode;
                                    newError.MoodleEventId = mEventID;
                                    AddReportedMessage(newError);
                                }
                            }
                        }

                    }
                    else
                    {
                        
                        // creating new user in moodle
                        // enrolling him to the current event
                        Library.WriteErrorLog(string.Format("10a. CheckEvent() - newContact doesn't exist in Moodle - Code :{0}, User : {1} - Step 4a ", Escape(eventCode), Escape(order.ID)));

                        var success = new List<ServiceResultSuccess>();
                        ServiceResultError errorResult = null;
                        var newContact = GetContact(order.ID);
                        Library.WriteErrorLog(string.Format("11. CheckEvent() - GetContact - Code :{0}, User : {1} - Step 4a ", Escape(eventCode), Escape(order.ID)));
                        //if (debugEnabled) lblDebug.Text += string.Format("newContact ID: {0}<br />", newContact.iMISID);

                        // new logic, it seems email is very important parameter to create contact in moodle
                        if (string.IsNullOrEmpty(newContact.Email))
                        {
                            Library.WriteErrorLog(string.Format("12e. Event - Code :{0}, User : {1}, moodle Contact ID: {2} - Step 6, user doesn't have email address in iMIS, their credentials weren't created in Moodle, step skipped ", Escape(eventCode), Escape(order.ID), Escape(newContact.iMISID)), true);
                            var newError = new ReportedError();
                            newError.ErrorType = ReportErrorType.MissingEmail;
                            newError.ErrorMessage = "Member doesn't have email address in iMIS";
                            newError.MemberID = order.ID;
                            newError.MeetCode = eventCode;
                            newError.MoodleEventId = mEventID;
                            AddReportedMessage(newError);
                        }
                        else if (!string.IsNullOrEmpty(newContact.WebLogin))
                        {
                            var mContact = getMoodleContactByEmail(order.ID, newContact.Email);
                            if (mContact!= null)
                            {
                                Library.WriteErrorLog(string.Format("12e. Error - MainService().CheckEvent().getMoodleContactByEmail(). Email exists in Moodle: Id='{0}', EventCode='{1}', Email='{2}'", order.ID, eventCode, newContact.Email), true);
                                var newError = new ReportedError();
                                newError.ErrorType = ReportErrorType.DuplicateEmail;
                                newError.ErrorMessage = "Member cannot be registred in Moodle, member's email exists: " + newContact.Email;
                                newError.MemberID = order.ID;
                                newError.MeetCode = eventCode;
                                newError.MoodleEventId = mEventID;
                                AddReportedMessage(newError);
                                
                            }
                            else
                            {
                                Library.WriteErrorLog(string.Format("12. CheckEvent() - CreateMoodleContact - Code :{0}, User : {1} - Step 4a ", Escape(eventCode), Escape(order.ID)));
                                string newRequest = string.Empty;
                                string result = CreateMoodleContact(newContact, out newRequest);

                                if (!string.IsNullOrEmpty(result))
                                {
                                    try
                                    {
                                        if (!result.Contains("exception"))
                                        {
                                            DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(List<ServiceResultSuccess>));
                                            using (var ms = new MemoryStream(Encoding.Unicode.GetBytes(result)))
                                            {
                                                success = (List<ServiceResultSuccess>)serializer.ReadObject(ms);
                                                Library.WriteErrorLog(string.Format("12c. CheckEvent() - CreateMoodleContact.result = success - Code :{0}, User : {1}, success.Count: {2} - Step 4a ", Escape(eventCode), Escape(order.ID), success.Count));
                                            }
                                        }
                                    }
                                    catch { }
                                    try
                                    {
                                        if (result.Contains("exception"))
                                        {
                                            DataContractJsonSerializer serializer2 = new DataContractJsonSerializer(typeof(ServiceResultError));
                                            using (var ms = new MemoryStream(Encoding.Unicode.GetBytes(result)))
                                            {
                                                errorResult = (ServiceResultError)serializer2.ReadObject(ms);
                                                Library.WriteErrorLog(string.Format("12c. CheckEvent() - CreateMoodleContact.result = error - Code :{0}, User : {1}, errorResult.message: {2} - Step 4a ", Escape(eventCode), Escape(order.ID), errorResult.message));
                                            }

                                        }
                                    }
                                    catch { }

                                }
                                if (success != null && success.Count > 0)
                                {
                                    var firstSuccess = success.FirstOrDefault();
                                    Library.WriteErrorLog(string.Format("12d. CheckEvent() - CreateMoodleContact.result = firstSuccess - Code :{0}, User : {1}, firstSuccess.username: {2} - Step 4a ", Escape(eventCode), Escape(order.ID), firstSuccess.username));
                                    //if (debugEnabled) lblDebug.Text += string.Format("New user has been created! ID = {0}, username={1}<br />", firstSuccess.id, firstSuccess.username);
                                }
                                if (errorResult != null) // && errorResult.Count > 0
                                {
                                    //var firstError = errorResult.FirstOrDefault();

                                    //error on moodle registration
                                    Library.WriteErrorLog(string.Format("12d. Error - MainService().CheckEvent().CreateMoodleContact().ResultError: Id='{0}', Event='{1}', Exception.exception='{2}', Exception.message='{3}', Exception.errorcode='{4}'", order.ID, eventCode, errorResult.exception, errorResult.message, errorResult.errorcode), true);
                                    var newError = new ReportedError();
                                    newError.ErrorType = ReportErrorType.MoodleServiceError;
                                    newError.ErrorMessage = "Moodle System Error on creating Moodle account: " + errorResult.message;
                                    newError.MemberID = order.ID;
                                    newError.MeetCode = eventCode;
                                    newError.MoodleEventId = mEventID;
                                    AddReportedMessage(newError);
                                }
                            }
                            
                        }
                        else
                        {
                            Library.WriteErrorLog(string.Format("12e. Event - Code :{0}, User : {1}, moodle Contact ID: {2} - Step 6, user doesn't have credentials in iMIS, their credentials weren't created in Moodle, step skipped ", Escape(eventCode), Escape(order.ID), Escape(newContact.iMISID)), true);
                            var newError = new ReportedError();
                            newError.ErrorType = ReportErrorType.MissingWebLogin;
                            newError.ErrorMessage = "Member doesn't have credentials in iMIS";
                            newError.MemberID = order.ID;
                            newError.MeetCode = eventCode;
                            newError.MoodleEventId = mEventID;
                            AddReportedMessage(newError);
                            // ErrorsList.Add(newError); // can't add directly as will have duplicates over time
                            //AddReportedMessage(newError);

                        }

                    }
                }
            }
        }

        #endregion

        #region Data Access Method

        public List<iEvent> GetAllEvents()
        {
            var list = new List<iEvent>();
            string sqlQuery = "select MEETING, TITLE from Meet_Master where STATUS IN ('A', 'F') and MEETING_TYPE IN ('ONLIN', 'COURS', 'WEBIN', 'EVENT')" +
                " AND (BEGIN_DATE < GETDATE() OR BEGIN_DATE IS NULL)";
            var db = new clsDB();
            var table = db.GetData(sqlQuery);

            if (table != null && table.Rows.Count > 0)
            {
                foreach (DataRow row in table.Rows)
                {
                    var item = new iEvent();
                    item.MeetingCode = (string)row["MEETING"];
                    item.MeetingTitle = (string)row["TITLE"];
                    list.Add(item);
                }
            }
            return list;
        }

        public List<iOrder> GetOrders(string EventKey)
        {
            var list = new List<iOrder>();
            string sqlQuery = "SELECT Orders.ORDER_NUMBER, Orders.ORDER_DATE, Order_Meet.MEETING, Orders.ST_ID FROM Orders " + 
                    " LEFT OUTER JOIN Order_Meet ON Order_Meet.ORDER_NUMBER = Orders.ORDER_NUMBER " +
                    " LEFT OUTER JOIN Activity ON Activity.ID = Orders.ST_ID AND Activity.PRODUCT_CODE = Order_Meet.MEETING  " +
                    " AND Activity.ACTIVITY_TYPE='CRS_RESULT' AND Activity.ACTION_CODES = 'PASSED' " +
                    " WHERE Orders.STATUS NOT IN ('C', 'CT') AND Order_Meet.MEETING = " + Escape(EventKey) + " AND activity.SEQN IS NULL AND Orders.BALANCE<=0 ";
            var db = new clsDB();
            var table = db.GetData(sqlQuery);

            if (table != null && table.Rows.Count > 0)
            {
                foreach (DataRow row in table.Rows)
                {
                    var item = new iOrder();
                    item.OrderNum = (int)(decimal)row["ORDER_NUMBER"];
                    item.ID = (string)row["ST_ID"];
                    if (row["ORDER_DATE"]!=DBNull.Value) item.OrderDate = (DateTime)row["ORDER_DATE"];
                    list.Add(item);
                }
            }
            return list;
        }

        public iOrder GetPaidOrder(string EventKey, string userID)
        {
            string _connString = ConfigurationManager.ConnectionStrings["SqlServerConnection"].ToString();
            iOrder order = null;
            //string dbconn = _ConnString;
            var db = new clsDB();
            string sqlQuery = "sp_VA_GetEventStatus";
            var connection = new SqlConnection(_connString);
            SqlCommand command = new SqlCommand(sqlQuery, connection);
            command.CommandType = CommandType.StoredProcedure;

            command.Parameters.Add(new SqlParameter("@EventKey", SqlDbType.VarChar, 10));
            command.Parameters["@EventKey"].Value = EventKey;

            command.Parameters.Add(new SqlParameter("@IMIS_ID", SqlDbType.VarChar, 10));
            command.Parameters["@IMIS_ID"].Value = userID;

            var table = db.ExecuteReader(command);

            if (table != null && table.Rows.Count > 0)
            {
                DataRow row = table.Rows[0];
                order = new iOrder();
                order.OrderNum = (int)(decimal)row["ORDER_NUM"];
                order.ID = (string)row["ST_ID"];
                if (row["ORDER_DATE"] != DBNull.Value) order.OrderDate = (DateTime)row["ORDER_DATE"];
            }
            return order;
        }

        public modContact GetContact(string userID)
        {
            //message = string.Empty;
            modContact iContact = null;
            //Asi.iBO.ContactManagement.CContact contact;
            //IiMISUser user;

            string sqlQuery = "SELECT Name.ID, Name.First_Name, Name.Last_Name, Name.Email, LOWER(UserMain.UserId) as WebLogin FROM Name " + 
                " LEFT OUTER JOIN UserMain On UserMain.ContactMaster = Name.ID " +
                " WHERE Name.Id = " + Escape(userID);
            var db = new clsDB();
            var table = db.GetData(sqlQuery);

            if (table != null && table.Rows.Count == 1)
            {
                DataRow row = table.Rows[0];
                iContact = new modContact();
                iContact.iMISID = userID;
                iContact.Email = (string)row["Email"];
                iContact.FirstName = (string)row["First_Name"];
                iContact.LastName = (string)row["Last_Name"];
                if (row["WebLogin"] != DBNull.Value) iContact.WebLogin = (string)row["WebLogin"];
            }
            return iContact;
        }

        public string CreateMoodleContact(modContact newContact, out string request)
        {
            request = string.Empty;
            string functionName = "core_user_create_users";
            string emailParam = string.Empty;
            if (!string.IsNullOrEmpty(newContact.Email)) emailParam = string.Format("&users[0][email]={0}", newContact.Email);
            string newPassword = string.Format("m@{0}{1}{2}", newContact.iMISID, newContact.FirstName.Substring(0, 1), newContact.LastName.Substring(0, 1));
            string functionParams = string.Format("users[0][username]={0}&users[0][password]={1}&users[0][firstname]={2}&users[0][lastname]={3}{4}&users[0][idnumber]={5}",
                newContact.WebLogin.ToLower(), newPassword, newContact.FirstName, newContact.LastName, emailParam, newContact.iMISID);
            // moodlesandbox10.lambdasolutions.net/webservice/rest/server.php?wstoken=345dee5e1d5e7f7b9272013dead23c33&wsfunction=core_user_create_users&moodlewsrestformat=json&
            //users[0][username]=user1&
            //users[0][password]=tesT12345!&
            //users[0][firstname]=FirstName1&
            //users[0][lastname]=LastName1&
            //users[0][email]=test.test@test.com
            //users[0][idnumber]=test.test@test.com


            string requestUrl = string.Format("{0}?wstoken={1}&wsfunction={2}&moodlewsrestformat=json&{3}", webserviceurl, token, functionName, functionParams);
            request = requestUrl;
            var syncClient = new WebClient();
            var content = syncClient.DownloadString(requestUrl);

            Library.WriteErrorLog(string.Format("12a. [Moodle]CreateMoodleContact(), RequestURL: {0}, Request On: {1}", requestUrl, DateTime.Now));
            //lblRequest.Text = requestUrl;
            //lblResponse.Text = content;
            return content;
        }

        // --- Activity_type='CRS_RESULT', TRANSACTION_DATE=TODAY(), PRODUCT_CODE = 'EDOTCM16' (MEETING), ACTION_CODES = 'PASSED'/'FAILED', TICKLER_DATE = [TAKING_DATE], UF_1 = OrderNum
        protected void AddCompetionStatus(string EventKey, string userID, DateTime orderDate, int orderNum )
        {
            var existing = GetCompletedtatus(EventKey, userID, orderNum);
            if (!existing)
            {
                int newSEQN = GetCounter("Activity");
                var db = new clsDB();
                string sql = string.Format("INSERT INTO Activity(ID, SEQN, Activity_Type, TRANSACTION_DATE, PRODUCT_CODE, Action_Codes, TICKLER_DATE, UF_1 ) " +
                    " VALUES({0}, {1}, 'CRS_RESULT', GETDATE(), {2}, 'PASSED', {3}, {4}) ", Escape(userID), newSEQN, Escape(EventKey), Escape(orderDate.ToString("yyyy-MM-dd")), Escape(orderNum.ToString()));
                db.ExecuteCommand(sql);
            }
        }

        protected bool CheckCompletionStatusExists(string EventKey, string userID, DateTime orderDate, int orderNum)
        {
            bool result = false;
            string sql = string.Format("SELECT top(1) SEQN FROM Activity WHERE ID = {0} AND Activity_Type = 'CRS_RESULT' AND Action_Codes='PASSED' AND PRODUCT_CODE = {1} AND UF_1 = {2}", Escape(userID), Escape(EventKey), Escape(orderNum.ToString()));
            return result;
        }

        protected void AddFailedStatus(string EventKey, string userID, DateTime orderDate, int orderNum)
        {
            var existing = GetFailedStatus(EventKey, userID);
            if (!existing)
            {
                int newSEQN = GetCounter("Activity");
                var db = new clsDB();
                string sql = string.Format("INSERT INTO Activity(ID, SEQN, Activity_Type, TRANSACTION_DATE, PRODUCT_CODE, Action_Codes, TICKLER_DATE, UF_1 ) " +
                    " VALUES({0}, {1}, 'CRS_RESULT', GETDATE(), {2}, 'FAILED', {3}, {4}) ", Escape(userID), newSEQN, Escape(EventKey), Escape(orderDate.ToString("yyyy-MM-dd")), Escape(orderNum.ToString()));
                db.ExecuteCommand(sql);
            }
        }

        public bool GetFailedStatus(string EventKey, string userID)
        {
            bool retValue = false;
            var db = new clsDB();
            string sql = string.Format("SELECT SEQN FROM Activity WHERE ID = {0} AND Activity_Type = 'CRS_RESULT' AND PRODUCT_CODE = {1} AND Action_Codes = 'FAILED' ",
                Escape(userID), Escape(EventKey));
            var table = db.GetData(sql);
            if (table != null && table.Rows.Count > 0)
            {
                retValue = true;
            }
            return retValue;
        }

        public bool GetCompletedtatus(string EventKey, string userID, int orderNum)
        {
            bool retValue = false;
            var db = new clsDB();
            string sql = string.Format("SELECT SEQN FROM Activity WHERE ID = {0} AND Activity_Type = 'CRS_RESULT' AND PRODUCT_CODE = {1} AND Action_Codes = 'PASSED' AND UF_1 = {2} ",
                Escape(userID), Escape(EventKey), Escape(orderNum.ToString()));
            var table = db.GetData(sql);
            if (table != null && table.Rows.Count > 0)
            {
                retValue = true;
            }
            return retValue;
        }

        public FullCoursesResult getMoodleAllCourses()
        {
            Library.WriteErrorLog(string.Format("MainService, getMoodleAllCourses started"));
            ObjectCache cache = MemoryCache.Default;
            FullCoursesResult result = cache["FullCoursesResult"] as FullCoursesResult; // null;
            try
            {

                if (result == null)
                {
                    string functionName = "core_course_get_courses";
                    //string fieldname = "userid";
                    string requestUrl = string.Format("{0}?wstoken={1}&wsfunction={2}&moodlewsrestformat=json", webserviceurl, token, functionName);

                    //ServicePointManager.Expect100Continue = true;
                    ////ServicePointManager.DefaultConnectionLimit = 9999;
                    ////ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12; // | SecurityProtocolType.Tls | SecurityProtocolType.Tls11;
                    ////                                                                   // Skip validation of SSL/TLS certificate // (SecurityProtocolType)3072; ;
                    ////ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

                    ////ServicePointManager.Expect100Continue = true;
                    ////ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                    ////| SecurityProtocolType.Tls11
                    ////| SecurityProtocolType.Tls12
                    ////| SecurityProtocolType.Ssl3;
                    //ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                    //var content = string.Empty;
                    //var syncClient = new WebClient();
                    //content = syncClient.DownloadString(requestUrl);

                    var content = GetMoodleRequest(requestUrl);
                    //var httpWebRequest = (HttpWebRequest)WebRequest.Create(requestUrl);
                    //httpWebRequest.ContentType = "application/json";
                    //httpWebRequest.Method = "GET";
                    //httpWebRequest.ProtocolVersion = HttpVersion.Version10;
                    //var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                    //using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                    //{
                    //    content = streamReader.ReadToEnd();
                    //}

                        Library.WriteErrorLog(string.Format("2a. [Moodle]getMoodleAllCourses()[non-cached] - requestUrl: {0}, Request On: {1}", requestUrl, DateTime.Now));

                    // Create the Json serializer and parse the response
                    if (!string.IsNullOrEmpty(content))
                    {
                        result = new FullCoursesResult();
                        if (!content.Contains("exception"))
                        {
                            result.ResultSuccess = new List<FullCoursesResultSuccess>();
                            DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(List<FullCoursesResultSuccess>));

                            using (var ms = new MemoryStream(Encoding.Unicode.GetBytes(content)))
                            {
                                result.ResultSuccess = (List<FullCoursesResultSuccess>)serializer.ReadObject(ms);
                            }
                        }
                        else
                        {
                            result.ResultError = new FullCoursesResultError();
                            DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(List<FullCoursesResultError>));

                            using (var ms = new MemoryStream(Encoding.Unicode.GetBytes(content)))
                            {
                                result.ResultError = (FullCoursesResultError)serializer.ReadObject(ms);
                            }
                        }
                    }

                    var expiration = DateTimeOffset.UtcNow.AddMinutes(5);
                    cache.Add("FullCoursesResult", result, expiration);
                }
                else
                {
                    Library.WriteErrorLog(string.Format(" getMoodleAllCourses()[cached] - Request On: {0}", DateTime.Now));
                }
            }
            catch(Exception ex)
            {
                Library.WriteErrorLog(string.Format("MainService, getMoodleAllCourses error, ex.source:{0}, message: {1}, inner: {2}, stack: {3}", ex.Source, ex.Message, ex.InnerException, ex.StackTrace));
            }

            Library.WriteErrorLog(string.Format("MainService, getMoodleAllCourses completed"));
            return result;
        }

        private string GetMoodleRequest(string myRequest){
            var result = string.Empty;

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            WebRequest request = WebRequest.Create(myRequest);
            // Set the Method property of the request to POST.
            request.Method = "GET";
            request.ContentType = "application/x-www-form-urlencoded";
            // Set the ContentLength property of the WebRequest.
            //request.ContentLength = byteArray.Length;
            //string encoded = System.Convert.ToBase64String(Encoding.GetEncoding("ISO-8859-1")
            //                   .GetBytes(username + ":" + password));
            //request.Headers.Add("Authorization", "Basic " + encoded);
            // Get the response.
            WebResponse response = request.GetResponse();
            // Display the status.
            //Console.WriteLine(((HttpWebResponse)response).StatusDescription);
            // Get the stream containing content returned by the server.
            // The using block ensures the stream is automatically closed.
            using (var dataStream = response.GetResponseStream())
            {
                // Open the stream using a StreamReader for easy access.
                StreamReader reader = new StreamReader(dataStream);
                // Read the content.
                string responseFromServer = reader.ReadToEnd();
                // Display the content.
                //Console.WriteLine(responseFromServer);
                JavaScriptSerializer js = new JavaScriptSerializer();
                //dynamic responseObj = (dynamic)js.Deserialize(responseFromServer, typeof(dynamic));
                result = responseFromServer;
            }

            return result;
        }

        [DataContract]
        public class PostResponse
        {
            [DataMember]
            public string status { get; set; }
            [DataMember]
            public string message { get; set; }
            [DataMember]
            public string data { get; set; }
        }
        //private static void AddHeader(SqlChars H, WebClient client)
        //{
        //    if (!H.IsNull)
        //    {
        //        string header = H.ToString();
        //        if (!IsNullOrWhiteSpace(header))
        //            client.Headers.Add(HttpRequestHeader.UserAgent, header);
        //    }
        //}

        //public static bool IsNullOrWhiteSpace(this string theString)
        //{
        //    if (theString == null)
        //    {
        //        return false;
        //    }

        //    if (theString.Trim() == string.Empty)
        //    {
        //        return false;
        //    }
        //    return true;
        //}

        public CourseCompletionResult getMoodleCourseResult(string MoodleID, string CourseID)
        {
            CourseCompletionResult result = null;
            string functionName = "core_completion_get_course_completion_status";
            //string fieldname = "userid";
            string requestUrl = string.Format("{0}?wstoken={1}&wsfunction={2}&moodlewsrestformat=json&courseid={3}&userid={4}", webserviceurl, token, functionName, CourseID, MoodleID);
            Library.WriteErrorLog(string.Format("13a. MoodleID :{0}, CourseID : {1},requestUrl: {2} - Step 7 ", Escape(MoodleID), Escape(CourseID), Escape(requestUrl)));
            var syncClient = new WebClient();
            var content = syncClient.DownloadString(requestUrl);

            //Library.WriteErrorLog(string.Format("[Moodle]getMoodleCourseResult() - Request On: {0}", DateTime.Now));

            if (!string.IsNullOrEmpty(content))
            {
                result = new CourseCompletionResult();
                if (!content.Contains("exception"))
                {
                    result.ResultSuccess = new CourseCompletionResultSuccess();
                    DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(CourseCompletionResultSuccess));

                    using (var ms = new MemoryStream(Encoding.Unicode.GetBytes(content)))
                    {
                        result.ResultSuccess = (CourseCompletionResultSuccess)serializer.ReadObject(ms);
                    }
                }
                else
                {
                    result.ResultError = new CourseCompletionResultError();
                    DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(CourseCompletionResultError));

                    using (var ms = new MemoryStream(Encoding.Unicode.GetBytes(content)))
                    {
                        result.ResultError = (CourseCompletionResultError)serializer.ReadObject(ms);
                    }
                }
            }
            return result;
        }

        public List<moodleUser> getMoodleContact(string userID, string fieldname)
        {
            List<moodleUser> iContacts = GetMoodleContactInImis(userID);

            if (iContacts == null)
            {
                string functionName = "core_user_get_users_by_field";
                //string fieldname = "idnumber";
                string requestUrl = string.Format("{0}?wstoken={1}&wsfunction={2}&moodlewsrestformat=json&field={3}&values[0]={4}", webserviceurl, token, functionName, fieldname, userID);

                var syncClient = new WebClient();
                var content = syncClient.DownloadString(requestUrl);

                Library.WriteErrorLog(string.Format("9b. [Moodle]getMoodleContact(), RequestURL: {0}, Request On: {1}", requestUrl, DateTime.Now));

                // Create the Json serializer and parse the response
                if (!string.IsNullOrEmpty(content))
                {
                    DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(List<moodleUser>));

                    using (var ms = new MemoryStream(Encoding.Unicode.GetBytes(content)))
                    {
                        iContacts = (List<moodleUser>)serializer.ReadObject(ms);
                        // contac exists in Moodle but no records in Moodle_MemberRegistrations, creting new one.
                        if (iContacts != null)
                        {
                            var firstContact = iContacts.FirstOrDefault();
                            if (firstContact!=null) CreateMoodleContactInImis(userID, firstContact.id);
                            Library.WriteErrorLog(string.Format("9c. [IMIS]CreateMoodleContactInImis(), userID: {0}, createdOn: {1}", userID, DateTime.Now));
                        }
                    }
                }
            }
            
            return iContacts;
        }

        public List<moodleUser> getMoodleContactByEmail(string userID, string email)
        {
            List<moodleUser> iContacts = null;

            //if (iContacts == null)
            //{
                string functionName = "core_user_get_users_by_field";
                //string fieldname = "idnumber";
                string requestUrl = string.Format("{0}?wstoken={1}&wsfunction={2}&moodlewsrestformat=json&field={3}&values[0]={4}", webserviceurl, token, functionName, "email", email);

                var syncClient = new WebClient();
                var content = syncClient.DownloadString(requestUrl);

                Library.WriteErrorLog(string.Format("9c. [Moodle]getMoodleContactByEmail(), RequestURL: {0}, Request On: {1}", requestUrl, DateTime.Now));

                // Create the Json serializer and parse the response
                if (!string.IsNullOrEmpty(content))
                {
                    DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(List<moodleUser>));

                    using (var ms = new MemoryStream(Encoding.Unicode.GetBytes(content)))
                    {
                        iContacts = (List<moodleUser>)serializer.ReadObject(ms);
                        // contac exists in Moodle but no records in Moodle_MemberRegistrations, creting new one.
                        //if (iContacts != null)
                        //{
                        //    var firstContact = iContacts.FirstOrDefault();
                        //    //if (firstContact != null) CreateMoodleContactInImis(userID, firstContact.id);
                        //    Library.WriteErrorLog(string.Format("9c. [IMIS]CreateMoodleContactInImis(), userID: {0}, createdOn: {1}", userID, DateTime.Now));
                        //}
                    }
                }
            //}

            return iContacts;
        }

        public List<moodleUser> GetMoodleContactInImis(string userID)
        {
            List<moodleUser> retValue = null;
            var db = new clsDB();
            string sql = string.Format("SELECT TOP(1) * FROM MOODLE_MemberRegistrations WHERE ID = {0} ", Escape(userID));
            var table = db.GetData(sql);
            if (table != null && table.Rows.Count  == 1)
            {
                DataRow row = table.Rows[0];
                var newUser = new moodleUser();
                newUser.idnumber = userID;
                newUser.id = (int)row["MoodleID"];
                retValue = new List<moodleUser>();
                retValue.Add(newUser);
                Library.WriteErrorLog(string.Format("9a. GetMoodleContactInImis(), userID: {0}, MoodleID: {1}", userID, newUser.id));
            }
            return retValue;
        }

                       
        public CourseRegistrationInfo CheckMoodleCourseRegistrationInImis(string memberID, int MoodleCourseID)
        {
            CourseRegistrationInfo retValue = null;
            var db = new clsDB();
            string sql = string.Format("SELECT TOP(1) * FROM MOODLE_CourseRegistrations WHERE ID = {0} AND Moodle_EventID = {1} ", Escape(memberID), MoodleCourseID);
            var table = db.GetData(sql);
            if (table != null && table.Rows.Count == 1)
            {
                DataRow row = table.Rows[0];
                retValue = new CourseRegistrationInfo();
                retValue.ID = memberID;
                retValue.MoodleEventID = MoodleCourseID;
                retValue.Completed = (bool)row["Completed"];
            }
            Library.WriteErrorLog(string.Format("11a. CheckMoodleCourseRegistrationInImis - table.Rows.Count :{0}, ", table.Rows.Count));
            return retValue;
        }

        public void CreateMoodleContactInImis(string memberID, int MoodleID)
        {
            var db = new clsDB();
            string sql = string.Format("INSERT INTO MOODLE_MemberRegistrations(ID, CreatedOn, MoodleID) VALUES({0}, GetDATE(),{1})", Escape(memberID), MoodleID);
            db.ExecuteCommand(sql);
        }

        public void CreateMoodleCoourseRegistrationInImis(string memberID, string EventCode, int MoodleEventID)
        {
            var db = new clsDB();
            string sql = string.Format("INSERT INTO MOODLE_CourseRegistrations(ID, Meet_Code, Moodle_EventID, CreatedOn) VALUES({0}, {1}, {2}, GetDATE())", Escape(memberID), Escape(EventCode), MoodleEventID);
            db.ExecuteCommand(sql); 
        }

        public void MarkCourseCompleted(string memberID, string EventCode)
        {
            var db = new clsDB();
            string sql = string.Format("UPDATE MOODLE_CourseRegistrations SET Completed=1 WHERE ID={0} AND Meet_Code = {1}", Escape(memberID), Escape(EventCode));
            db.ExecuteCommand(sql);
        }

        public CoursesResult getMoodleCourses(string MoodleID)
        {
            CoursesResult result = null;
            string functionName = "core_enrol_get_users_courses";
            //string fieldname = "userid";
            string requestUrl = string.Format("{0}?wstoken={1}&wsfunction={2}&moodlewsrestformat=json&userid={3}", webserviceurl, token, functionName, MoodleID);

            var syncClient = new WebClient();
            var content = syncClient.DownloadString(requestUrl);
            Library.WriteErrorLog(string.Format("11b. [Moodle]getMoodleCourses(), RequestURL: {0}, Request On: {1}", requestUrl, DateTime.Now));

            if (!string.IsNullOrEmpty(content))
            {
                result = new CoursesResult();
                if (!content.Contains("exception"))
                {
                    result.ResultSuccess = new List<CoursesResultSuccess>();
                    DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(List<CoursesResultSuccess>));

                    using (var ms = new MemoryStream(Encoding.Unicode.GetBytes(content)))
                    {
                        result.ResultSuccess = (List<CoursesResultSuccess>)serializer.ReadObject(ms);
                    }
                }
                else
                {
                    result.ResultError = new CoursesResultError();
                    DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(List<CoursesResultError>));

                    using (var ms = new MemoryStream(Encoding.Unicode.GetBytes(content)))
                    {
                        result.ResultError = (CoursesResultError)serializer.ReadObject(ms);
                    }
                }
            }
            return result;
        }

        public EnrollmentResult getMoodleUserEnroledForCourse(string userID, string CourseID)
        {
            EnrollmentResult result = null;
            string functionName = "enrol_manual_enrol_users";
            //string fieldname = "userid";
            string func_params = string.Format("enrolments[0][roleid]=5&enrolments[0][userid]={0}&enrolments[0][courseid]={1}", userID, CourseID);
            string requestUrl = string.Format("{0}?wstoken={1}&wsfunction={2}&moodlewsrestformat=json&{3}", webserviceurl, token, functionName, func_params);

            var syncClient = new WebClient();
            var content = syncClient.DownloadString(requestUrl);

            Library.WriteErrorLog(string.Format("12a. [Moodle]getMoodleUserEnroledForCourse(), RequestURL: {0}, Request On: {1}", requestUrl, DateTime.Now));
            
            if (!content.Contains("exception"))
            {
                result = new EnrollmentResult();
                result.ResultSuccess = new EnrollmenResultSuccess();
                result.ResultSuccess.message = "Success!";
            }
            else
            {
                result = new EnrollmentResult();
                result.ResultError = new EnrollmentResultError();
                DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(EnrollmentResultError));

                using (var ms = new MemoryStream(Encoding.Unicode.GetBytes(content)))
                {
                    result.ResultError = (EnrollmentResultError)serializer.ReadObject(ms);
                }
            }
            return result;
        }

        public int GetCounter(string CounterName)
        {
            string _connString = ConfigurationManager.ConnectionStrings["SqlServerConnection"].ToString();
            string _ConnMain = _connString;

            int returnValue = 1;
            var ds = SQLHelper.ExecuteDataset(_ConnMain, "sp_asi_GetCounter", CounterName, 1, 0);
            if (ds != null && ds.Tables[0] != null && ds.Tables[0].Rows.Count == 1)
            {
                returnValue = (int)ds.Tables[0].Rows[0].ItemArray[0];
            }
            return returnValue;
        }

        protected string Escape(string val)
        {
            string str;
            if (string.IsNullOrEmpty(val))
            {
                val = string.Empty;
            }
            str = val.Replace("'", "''");
            str = "'" + str + "'";
            return str;
        }

        #endregion

        #region Email methods

        private void SendEmail()
        {
            if (ErrorsList!= null && ErrorsList.Count > 0)
            {
                MailConfiguration mc = new MailConfiguration();

                using (SmtpClient client = new SmtpClient(mc.Host, mc.Port))
                {
                    // Set SMTP client properties
                    client.EnableSsl = true;
                    client.UseDefaultCredentials = mc.DefaultCredentials;
                    client.Credentials = new NetworkCredential(mc.UserName, mc.Password);

                    // Send
                    string emailBody = string.Empty;
                    string emailBodyRow = string.Empty;
                    string ticketContent = GetEmailTemplateContent("EmailTemplate.html");
                    string ticketContentDetail = GetEmailTemplateContent("EmailTemplateRow.html");
                    foreach (var msg in ErrorsList)
                    {
                        emailBodyRow += ticketContentDetail.Replace("{{ErrorType}}", msg.ErrorType.ToString()).
                            Replace("{{ErrorMessage}}", msg.ErrorMessage).
                            Replace("{{MemberID}}", msg.MemberID).
                            Replace("{{MoodleID}}", msg.MoodleID.ToString()).
                            Replace("{{MeetCode}}", msg.MeetCode).
                            Replace("{{MoodleEventId}}", msg.MoodleEventId.ToString());

                    }
                    emailBody = ticketContent.Replace("{{insertrow}}", emailBodyRow);
                    try
                    {
                        var mailMessage = new MailMessage();
                        mailMessage.From = new MailAddress(mc.FromAddress);
                        var sContacts = emailAddresses.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
                        for (int i = 0; i < sContacts.Length; i++)
                        {
                            mailMessage.To.Add(new MailAddress(sContacts[i]));
                        }
                        
                        mailMessage.Body = emailBody;
                        mailMessage.Subject = "ONPHA Moodle Service report";
                        mailMessage.IsBodyHtml = true;

                        client.Send(mailMessage);
                        Library.WriteErrorLog(string.Format("SendEmail() - Done, Exception: {0}", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
                    }
                    catch (Exception ex)
                    { 
                        Library.WriteErrorLog(string.Format("SendEmail. Error - SendEmail(), Exception: {1}", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), ex.Message), true);
                    }
                    finally
                    {
                        client.Dispose();
                    }
                }
                ErrorsList = new List<ReportedError>();
            }
            
        }

        public class MailConfiguration
        {
            private SmtpSection smtpSection = (ConfigurationManager.GetSection("system.net/mailSettings/smtp") as SmtpSection);
            /// <summary> 
            /// Location of configuration file 
            /// </summary> 
            public string ConfigurationFileName
            {
                get
                {
                    try
                    {
                        return smtpSection.ElementInformation.Source;
                    }
                    catch (Exception)
                    {

                        return "";
                    }

                }
            }
            /// <summary> 
            /// Email address for the system 
            /// </summary> 
            public string FromAddress { get { return smtpSection.From; } }
            /// <summary> 
            /// Gets the name or IP address of the host used for SMTP transactions. 
            /// </summary> 
            public string Host { get { return smtpSection.Network.Host; } }
            /// <summary> 
            /// Gets the port used for SMTP transactions 
            /// </summary> 
            /// <remarks>default host is 25</remarks> 
            public int Port { get { return smtpSection.Network.Port; } }
            /// <summary> 
            /// Gets a value that specifies the amount of time after  
            /// which a synchronous Send call times out. 
            /// </summary> 
            public int TimeOut { get { return 2000; } }
            public string UserName { get { return smtpSection.Network.UserName; } }
            public string Password { get { return smtpSection.Network.Password; } }
            public bool DefaultCredentials { get { return smtpSection.Network.DefaultCredentials; } }
            public override string ToString()
            {
                return "From: [" + FromAddress + "] Host: [" + Host + "] Port: [" + Port + "]";
            }
        }

        private static string GetEmailTemplateContent(string filename)
        {
            string fileName = Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory.ToString(), filename);
            string readText = File.ReadAllText(fileName);
            return readText;
        }
        #endregion


        #region Internal Classes

        public class iEvent
        {
            public string MeetingCode { get; set; }
            public string MeetingTitle { get; set; }
            public bool moodleEvent { get; set; }
            public int moodleEventId { get; set; }
        }

        public class iOrder
        {
            public int OrderNum { get; set; }
            public string ID { get; set; }
            public DateTime OrderDate { get; set; }
        }

        public class FullCoursesResult
        {
            public List<FullCoursesResultSuccess> ResultSuccess { get; set; }
            public FullCoursesResultError ResultError { get; set; }
        }

        [DataContract]
        public class FullCoursesResultSuccess
        {
            [DataMember]
            public int id { get; set; }
            [DataMember]
            public string shortname { get; set; }
            [DataMember]
            public int categoryid { get; set; }
            [DataMember]
            public int categorysortorder { get; set; }
            [DataMember]
            public string fullname { get; set; }
            [DataMember]
            public string displayname { get; set; }
            [DataMember]
            public string idnumber { get; set; }
            [DataMember]
            public string summary { get; set; }
            [DataMember]
            public int summaryformat { get; set; }
            [DataMember]
            public string format { get; set; }
            [DataMember]
            public int showgrades { get; set; }
            [DataMember]
            public int newsitems { get; set; }
            [DataMember]
            public int startdate { get; set; }
            [DataMember]
            public int enddate { get; set; }
            [DataMember]
            public int numsections { get; set; }
            [DataMember]
            public int maxbytes { get; set; }
            [DataMember]
            public int showreports { get; set; }
            [DataMember]
            public int visible { get; set; }
            [DataMember]
            public int groupmode { get; set; }
            [DataMember]
            public int groupmodeforce { get; set; }
            [DataMember]
            public int defaultgroupingid { get; set; }
            [DataMember]
            public int timecreated { get; set; }
            [DataMember]
            public int timemodified { get; set; }
            [DataMember]
            public int enablecompletion { get; set; }
            [DataMember]
            public int completionnotify { get; set; }
            [DataMember]
            public string lang { get; set; }
            [DataMember]
            public string forcetheme { get; set; }
            [DataMember]
            public List<Courseformatoption> courseformatoptions { get; set; }
            [DataMember]
            public int? hiddensections { get; set; }
        }

        [DataContract]
        public class FullCoursesResultError
        {
            [DataMember]
            public string exception { get; set; }
            [DataMember]
            public string errorcode { get; set; }
            [DataMember]
            public string message { get; set; }
            [DataMember]
            public string debuginfo { get; set; }
        }

        public class CoursesResult
        {
            public List<CoursesResultSuccess> ResultSuccess { get; set; }
            public CoursesResultError ResultError { get; set; }
        }

        [DataContract]
        public class CoursesResultSuccess
        {
            [DataMember]
            public int id { get; set; }
            [DataMember]
            public string shortname { get; set; }
            [DataMember]
            public string fullname { get; set; }
            [DataMember]
            public int enrolledusercount { get; set; }
            [DataMember]
            public string idnumber { get; set; }
            [DataMember]
            public int visible { get; set; }
            [DataMember]
            public string summary { get; set; }
            [DataMember]
            public int summaryformat { get; set; }
            [DataMember]
            public string format { get; set; }
            [DataMember]
            public bool showgrades { get; set; }
            [DataMember]
            public string lang { get; set; }
            [DataMember]
            public bool enablecompletion { get; set; }
        }

        [DataContract]
        public class CoursesResultError
        {
            [DataMember]
            public string exception { get; set; }
            [DataMember]
            public string errorcode { get; set; }
            [DataMember]
            public string message { get; set; }
            [DataMember]
            public string debuginfo { get; set; }
        }

        public class Courseformatoption
        {
            public string name { get; set; }
            public object value { get; set; }
        }

        public class CourseCompletionResult
        {
            public CourseCompletionResultSuccess ResultSuccess { get; set; }
            public CourseCompletionResultError ResultError { get; set; }
        }

        [DataContract]
        public class CourseCompletionResultError
        {
            [DataMember]
            public string exception { get; set; }
            [DataMember]
            public string errorcode { get; set; }
            [DataMember]
            public string message { get; set; }
            //[DataMember]
            //public string debuginfo { get; set; }
        }

        [DataContract]
        public class CourseCompletionResultSuccess
        {
            [DataMember]
            public Completionstatus completionstatus { get; set; }
            [DataMember]
            public List<object> warnings { get; set; }
        }

        [DataContract]
        public class CompletionDetails
        {
            [DataMember]
            public string type { get; set; }
            [DataMember]
            public string criteria { get; set; }
            [DataMember]
            public string requirement { get; set; }
            [DataMember]
            public string status { get; set; }
        }

        [DataContract]
        public class Completion
        {
            [DataMember]
            public int type { get; set; }
            [DataMember]
            public string title { get; set; }
            [DataMember]
            public string status { get; set; }
            [DataMember]
            public bool complete { get; set; }
            [DataMember]
            public object timecompleted { get; set; }
            [DataMember]
            public CompletionDetails details { get; set; }
        }

        [DataContract]
        public class Completionstatus
        {
            [DataMember]
            public bool completed { get; set; }
            [DataMember]
            public int aggregation { get; set; }
            [DataMember]
            public List<Completion> completions { get; set; }
        }

        public class modContact
        {
            public string iMISID { get; set; }
            public int moodleID { get; set; }
            //public long Row_Number { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string Email { get; set; }
            public string WebLogin { get; set; }
        }

        [DataContract]
        public class moodleUser
        {
            [DataMember]
            public int id { get; set; }
            [DataMember]
            public string username { get; set; }
            [DataMember]
            public string firstname { get; set; }
            [DataMember]
            public string lastname { get; set; }
            [DataMember]
            public string fullname { get; set; }
            [DataMember]
            public string email { get; set; }
            [DataMember]
            public string department { get; set; }
            [DataMember]
            public string idnumber { get; set; }
            [DataMember]
            public int firstaccess { get; set; }
            [DataMember]
            public int lastaccess { get; set; }
            [DataMember]
            public string description { get; set; }
            [DataMember]
            public int descriptionformat { get; set; }
            [DataMember]
            public string country { get; set; }
            [DataMember]
            public string profileimageurlsmall { get; set; }
            [DataMember]
            public string profileimageurl { get; set; }
            [DataMember]
            public List<Customfield> customfields { get; set; }
        }

        [DataContract]
        public class Customfield
        {
            [DataMember]
            public string type { get; set; }
            [DataMember]
            public string value { get; set; }
            [DataMember]
            public string name { get; set; }
            [DataMember]
            public string shortname { get; set; }
        }

        [DataContract]
        public class ServiceResultError
        {
            [DataMember]
            public string exception { get; set; }
            [DataMember]
            public string errorcode { get; set; }
            [DataMember]
            public string message { get; set; }
            //[DataMember]
            //public string debuginfo { get; set; }
        }

        [DataContract]
        public class ServiceResultSuccess
        {
            [DataMember]
            public int id { get; set; }
            [DataMember]
            public string username { get; set; }
        }

        public class EnrollmentResult
        {
            public EnrollmenResultSuccess ResultSuccess { get; set; }
            public EnrollmentResultError ResultError { get; set; }
        }

        public class EnrollmenResultSuccess
        {
            public string message { get; set; }
        }

        [DataContract]
        public class EnrollmentResultError
        {
            [DataMember]
            public string exception { get; set; }
            [DataMember]
            public string errorcode { get; set; }
            [DataMember]
            public string message { get; set; }
        }

        public enum ReportErrorType
        {
            MissingWebLogin,
            MissingEmail,
            DuplicateEmail,
            WindowsServiceError,
            MoodleServiceError
        }

        public class ReportedError
        {
            public ReportErrorType ErrorType { get; set; }
            public string ErrorMessage { get; set; }
            public string MemberID { get; set; }
            public int MoodleID { get; set; }
            public string MeetCode { get; set; }
            public int MoodleEventId { get; set; }

        }
        public class CourseRegistrationInfo
        {
            public string ID { get; set; }
            public string EventCode { get; set; }
            public int MoodleEventID { get; set; }
            public bool Completed { get; set; }
        }

        protected void AddReportedMessage(ReportedError message)
        {
            if (ErrorsList == null) ErrorsList = new List<ReportedError>();
            var find = ErrorsList.Find(i => i.ErrorType == message.ErrorType && i.MemberID == message.MemberID);
            if (find == null)
            {
                ErrorsList.Add(message);
            }
        }
        #endregion

        #region DataAccess Classes

        public class clsDB
        {
            //string _ConnString = WebConfigItems.GetConnectionString();
            //string _connString = ConfigurationManager.ConnectionStrings["SqlServerConnection"].ToString();
            string _ConnString = ConfigurationManager.ConnectionStrings["SqlServerConnection"].ToString(); // _connString;
            // public static string = System.Configuration.ConfigurationManager.ConnectionStrings["sqlConn"].ConnectionString;

            public clsDB()
            {
                //
                // TODO: Add constructor logic here
                //
            }

            //Execute the SQL
            public DataTable ExecuteReader(SqlCommand command)
            {
                DataTable dt = new DataTable();
                try
                {
                    command.Connection.Open();
                    SqlDataReader dr = command.ExecuteReader(CommandBehavior.CloseConnection);
                    dt.Load(dr);
                    return dt;
                }
                catch
                {
                    // handle error
                }
                finally
                {
                    command.Connection.Close();
                }
                return dt;
            }

            //Execute the SQL
            public bool ExecuteCommand(string strSQL)
            {
                string strConString = _ConnString;
                System.Data.SqlClient.SqlConnection conSQL = new System.Data.SqlClient.SqlConnection();
                conSQL.ConnectionString = strConString;
                SqlCommand cmdSQL = new SqlCommand();
                conSQL.Open();
                cmdSQL.Connection = conSQL;
                cmdSQL.CommandText = strSQL;
                cmdSQL.ExecuteNonQuery();
                conSQL.Close();
                conSQL.Dispose();
                return true;
            }


            //Get DataTable
            public DataTable GetData(string sql)
            {
                string strConString = _ConnString;
                DataTable dt = new DataTable();
                // try
                // {
                System.Data.SqlClient.SqlConnection conSQL = new System.Data.SqlClient.SqlConnection();
                conSQL.ConnectionString = strConString;
                System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter(sql, conSQL);
                DataSet datads = new DataSet();
                da.Fill(datads);
                da.Dispose();
                dt = datads.Tables[0];
                conSQL.Close();
                return dt;
            }

            //Get DataTable
            public DataTable GetDataSecured(string sql)
            {
                string strConString = _ConnString;
                DataTable dt = new DataTable();
                try
                {
                    System.Data.SqlClient.SqlConnection conSQL = new System.Data.SqlClient.SqlConnection();
                    conSQL.ConnectionString = strConString;
                    System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter(sql, conSQL);
                    DataSet datads = new DataSet();
                    da.Fill(datads);
                    da.Dispose();
                    dt = datads.Tables[0];
                    conSQL.Close();
                    return dt;

                }
                catch
                {
                    return dt;
                }
            }

            //Get single value STRING
            public string getStringFromDB(string sql)
            {
                string thisValue = "";
                using (SqlConnection conn = new SqlConnection(_ConnString))
                {
                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.Parameters.Add("@Name", SqlDbType.VarChar);
                    cmd.Parameters["@name"].Value = "TEST";
                    conn.Open();
                    try
                    {
                        thisValue = cmd.ExecuteScalar().ToString();
                    }
                    catch
                    {
                        thisValue = "";
                    }
                    conn.Close();
                }
                return thisValue;
            }
        }

        public class SQLHelper
        {
            //*********************************************************************
            //
            // Since this class provides only static methods, make the default constructor private to prevent 
            // instances from being created with "new SqlHelper()".
            //
            //*********************************************************************

            private SQLHelper() { }

            //*********************************************************************
            //
            // This method is used to attach array of SqlParameters to a SqlCommand.
            // 
            // This method will assign a value of DbNull to any parameter with a direction of
            // InputOutput and a value of null.  
            // 
            // This behavior will prevent default values from being used, but
            // this will be the less common case than an intended pure output parameter (derived as InputOutput)
            // where the user provided no input value.
            // 
            // param name="command" The command to which the parameters will be added
            // param name="commandParameters" an array of SqlParameters tho be added to command
            //
            //*********************************************************************

            private static void AttachParameters(SqlCommand command, SqlParameter[] commandParameters)
            {
                foreach (SqlParameter p in commandParameters)
                {
                    //check for derived output value with no value assigned
                    if ((p.Direction == ParameterDirection.InputOutput) && (p.Value == null))
                    {
                        p.Value = DBNull.Value;
                    }

                    command.Parameters.Add(p);
                }
            }

            //*********************************************************************
            //
            // This method assigns an array of values to an array of SqlParameters.
            // 
            // param name="commandParameters" array of SqlParameters to be assigned values
            // param name="parameterValues" array of objects holding the values to be assigned
            //
            //*********************************************************************

            private static void AssignParameterValues(SqlParameter[] commandParameters, object[] parameterValues)
            {
                if ((commandParameters == null) || (parameterValues == null))
                {
                    //do nothing if we get no data
                    return;
                }

                // we must have the same number of values as we pave parameters to put them in
                if (commandParameters.Length != parameterValues.Length)
                {
                    throw new ArgumentException("Parameter count does not match Parameter Value count.");
                }

                //iterate through the SqlParameters, assigning the values from the corresponding position in the 
                //value array
                for (int i = 0, j = commandParameters.Length; i < j; i++)
                {
                    commandParameters[i].Value = parameterValues[i];
                }
            }

            //*********************************************************************
            //
            // This method opens (if necessary) and assigns a connection, transaction, command type and parameters 
            // to the provided command.
            // 
            // param name="command" the SqlCommand to be prepared
            // param name="connection" a valid SqlConnection, on which to execute this command
            // param name="transaction" a valid SqlTransaction, or 'null'
            // param name="commandType" the CommandType (stored procedure, text, etc.)
            // param name="commandText" the stored procedure name or T-SQL command
            // param name="commandParameters" an array of SqlParameters to be associated with the command or 'null' if no parameters are required
            //
            //*********************************************************************

            private static void PrepareCommand(SqlCommand command, SqlConnection connection, SqlTransaction transaction, CommandType commandType, string commandText, SqlParameter[] commandParameters)
            {
                //if the provided connection is not open, we will open it
                if (connection.State != ConnectionState.Open)
                {
                    connection.Open();
                }

                //associate the connection with the command
                command.Connection = connection;

                //set the command text (stored procedure name or SQL statement)
                command.CommandText = commandText;

                //if we were provided a transaction, assign it.
                if (transaction != null)
                {
                    command.Transaction = transaction;
                }

                //set the command type
                command.CommandType = commandType;

                //attach the command parameters if they are provided
                if (commandParameters != null)
                {
                    AttachParameters(command, commandParameters);
                }


                return;
                //added KS Aug 2008
                //connection.Close();

            }

            //*********************************************************************
            //
            // Execute a SqlCommand (that returns no resultset) against the database specified in the connection string 
            // using the provided parameters.
            //
            // e.g.:  
            //  int result = ExecuteNonQuery(connString, CommandType.StoredProcedure, "PublishOrders", new SqlParameter("@prodid", 24));
            //
            // param name="connectionString" a valid connection string for a SqlConnection
            // param name="commandType" the CommandType (stored procedure, text, etc.)
            // param name="commandText" the stored procedure name or T-SQL command
            // param name="commandParameters" an array of SqlParamters used to execute the command
            // returns an int representing the number of rows affected by the command
            //
            //*********************************************************************

            public static int ExecuteNonQuery(string connectionString, CommandType commandType, string commandText, params SqlParameter[] commandParameters)
            {
                //create & open a SqlConnection, and dispose of it after we are done.
                using (SqlConnection cn = new SqlConnection(connectionString))
                {
                    cn.Open();

                    //call the overload that takes a connection in place of the connection string
                    // addition KS keith stoute august 2008  : 



                    return ExecuteNonQuery(cn, commandType, commandText, commandParameters);
                    //cn.Close();
                }
            }

            //*********************************************************************
            //
            // Execute a stored procedure via a SqlCommand (that returns no resultset) against the database specified in 
            // the connection string using the provided parameter values.  This method will query the database to discover the parameters for the 
            // stored procedure (the first time each stored procedure is called), and assign the values based on parameter order.
            // 
            // This method provides no access to output parameters or the stored procedure's return value parameter.
            // 
            // e.g.:  
            //  int result = ExecuteNonQuery(connString, "PublishOrders", 24, 36);
            //
            // param name="connectionString" a valid connection string for a SqlConnection
            // param name="spName" the name of the stored prcedure
            // param name="parameterValues" an array of objects to be assigned as the input values of the stored procedure
            // returns an int representing the number of rows affected by the command
            //
            //*********************************************************************

            public static int ExecuteNonQuery(string connectionString, string spName, params object[] parameterValues)
            {
                //if we receive parameter values, we need to figure out where they go
                if ((parameterValues != null) && (parameterValues.Length > 0))
                {
                    //pull the parameters for this stored procedure from the parameter cache (or discover them & populate the cache)
                    SqlParameter[] commandParameters = SqlHelperParameterCache.GetSpParameterSet(connectionString, spName);

                    //assign the provided values to these parameters based on parameter order
                    AssignParameterValues(commandParameters, parameterValues);

                    //call the overload that takes an array of SqlParameters
                    return ExecuteNonQuery(connectionString, CommandType.StoredProcedure, spName, commandParameters);
                }
                //otherwise we can just call the SP without params
                else
                {
                    return ExecuteNonQuery(connectionString, CommandType.StoredProcedure, spName);
                }
            }

            //*********************************************************************
            //
            // Execute a SqlCommand (that returns no resultset) against the specified SqlConnection 
            // using the provided parameters.
            // 
            // e.g.:  
            //  int result = ExecuteNonQuery(conn, CommandType.StoredProcedure, "PublishOrders", new SqlParameter("@prodid", 24));
            // 
            // param name="connection" a valid SqlConnection 
            // param name="commandType" the CommandType (stored procedure, text, etc.) 
            // param name="commandText" the stored procedure name or T-SQL command 
            // param name="commandParameters" an array of SqlParamters used to execute the command 
            // returns an int representing the number of rows affected by the command
            //
            //*********************************************************************

            public static int ExecuteNonQuery(SqlConnection connection, CommandType commandType, string commandText, params SqlParameter[] commandParameters)
            {
                //create a command and prepare it for execution
                SqlCommand cmd = new SqlCommand();
                PrepareCommand(cmd, connection, (SqlTransaction)null, commandType, commandText, commandParameters);

                //finally, execute the command.
                int retval = cmd.ExecuteNonQuery();

                // detach the SqlParameters from the command object, so they can be used again.
                cmd.Parameters.Clear();
                return retval;
            }

            //*********************************************************************
            //
            // Execute a SqlCommand (that returns a resultset) against the database specified in the connection string 
            // using the provided parameters.
            // 
            // e.g.:  
            //  DataSet ds = ExecuteDataset(connString, CommandType.StoredProcedure, "GetOrders", new SqlParameter("@prodid", 24));
            // 
            // param name="connectionString" a valid connection string for a SqlConnection 
            // param name="commandType" the CommandType (stored procedure, text, etc.) 
            // param name="commandText" the stored procedure name or T-SQL command 
            // param name="commandParameters" an array of SqlParamters used to execute the command 
            // returns a dataset containing the resultset generated by the command
            //
            //*********************************************************************

            public static DataSet ExecuteDataset(string connectionString, CommandType commandType, string commandText, params SqlParameter[] commandParameters)
            {
                //create & open a SqlConnection, and dispose of it after we are done.
                using (SqlConnection cn = new SqlConnection(connectionString))
                {
                    cn.Open();

                    //call the overload that takes a connection in place of the connection string
                    return ExecuteDataset(cn, commandType, commandText, commandParameters);
                    //cn.Close();
                }
            }

            //*********************************************************************
            //
            // Execute a stored procedure via a SqlCommand (that returns a resultset) against the database specified in 
            // the connection string using the provided parameter values.  This method will query the database to discover the parameters for the 
            // stored procedure (the first time each stored procedure is called), and assign the values based on parameter order.
            // 
            // This method provides no access to output parameters or the stored procedure's return value parameter.
            // 
            // e.g.:  
            //  DataSet ds = ExecuteDataset(connString, "GetOrders", 24, 36);
            // 
            // param name="connectionString" a valid connection string for a SqlConnection
            // param name="spName" the name of the stored procedure
            // param name="parameterValues" an array of objects to be assigned as the input values of the stored procedure
            // returns a dataset containing the resultset generated by the command
            //
            //*********************************************************************

            public static DataSet ExecuteDataset(string connectionString, string spName, params object[] parameterValues)
            {
                //if we receive parameter values, we need to figure out where they go
                if ((parameterValues != null) && (parameterValues.Length > 0))
                {
                    //pull the parameters for this stored procedure from the parameter cache (or discover them & populate the cache)
                    SqlParameter[] commandParameters = SqlHelperParameterCache.GetSpParameterSet(connectionString, spName);

                    //assign the provided values to these parameters based on parameter order
                    AssignParameterValues(commandParameters, parameterValues);

                    //call the overload that takes an array of SqlParameters
                    return ExecuteDataset(connectionString, CommandType.StoredProcedure, spName, commandParameters);
                }
                //otherwise we can just call the SP without params
                else
                {
                    return ExecuteDataset(connectionString, CommandType.StoredProcedure, spName);
                }
            }

            //*********************************************************************
            //
            // Execute a SqlCommand (that returns a resultset) against the specified SqlConnection 
            // using the provided parameters.
            // 
            // e.g.:  
            //  DataSet ds = ExecuteDataset(conn, CommandType.StoredProcedure, "GetOrders", new SqlParameter("@prodid", 24));
            //
            // param name="connection" a valid SqlConnection
            // param name="commandType" the CommandType (stored procedure, text, etc.)
            // param name="commandText" the stored procedure name or T-SQL command
            // param name="commandParameters" an array of SqlParamters used to execute the command
            // returns a dataset containing the resultset generated by the command
            //
            //*********************************************************************

            public static DataSet ExecuteDataset(SqlConnection connection, CommandType commandType, string commandText, params SqlParameter[] commandParameters)
            {
                //create a command and prepare it for execution
                SqlCommand cmd = new SqlCommand();
                PrepareCommand(cmd, connection, (SqlTransaction)null, commandType, commandText, commandParameters);

                //create the DataAdapter & DataSet
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();

                //fill the DataSet using default values for DataTable names, etc.
                da.Fill(ds);

                // detach the SqlParameters from the command object, so they can be used again.			
                cmd.Parameters.Clear();

                //return the dataset
                return ds;
            }

            //*********************************************************************
            //
            // Execute a SqlCommand (that returns a 1x1 resultset) against the database specified in the connection string 
            // using the provided parameters.
            // 
            // e.g.:  
            //  int orderCount = (int)ExecuteScalar(connString, CommandType.StoredProcedure, "GetOrderCount", new SqlParameter("@prodid", 24));
            // 
            // param name="connectionString" a valid connection string for a SqlConnection 
            // param name="commandType" the CommandType (stored procedure, text, etc.) 
            // param name="commandText" the stored procedure name or T-SQL command 
            // param name="commandParameters" an array of SqlParamters used to execute the command 
            // returns an object containing the value in the 1x1 resultset generated by the command
            //
            //*********************************************************************

            public static object ExecuteScalar(string connectionString, CommandType commandType, string commandText, params SqlParameter[] commandParameters)
            {
                //create & open a SqlConnection, and dispose of it after we are done.
                using (SqlConnection cn = new SqlConnection(connectionString))
                {
                    cn.Open();

                    //call the overload that takes a connection in place of the connection string
                    return ExecuteScalar(cn, commandType, commandText, commandParameters);
                    // cn.Close();
                }
            }

            //*********************************************************************
            //
            // Execute a stored procedure via a SqlCommand (that returns a 1x1 resultset) against the database specified in 
            // the connection string using the provided parameter values.  This method will query the database to discover the parameters for the 
            // stored procedure (the first time each stored procedure is called), and assign the values based on parameter order.
            // 
            // This method provides no access to output parameters or the stored procedure's return value parameter.
            // 
            // e.g.:  
            //  int orderCount = (int)ExecuteScalar(connString, "GetOrderCount", 24, 36);
            // 
            // param name="connectionString" a valid connection string for a SqlConnection 
            // param name="spName" the name of the stored procedure 
            // param name="parameterValues" an array of objects to be assigned as the input values of the stored procedure 
            // returns an object containing the value in the 1x1 resultset generated by the command
            //
            //*********************************************************************

            public static object ExecuteScalar(string connectionString, string spName, params object[] parameterValues)
            {
                //if we receive parameter values, we need to figure out where they go
                if ((parameterValues != null) && (parameterValues.Length > 0))
                {
                    //pull the parameters for this stored procedure from the parameter cache (or discover them & populate the cache)
                    SqlParameter[] commandParameters = SqlHelperParameterCache.GetSpParameterSet(connectionString, spName);

                    //assign the provided values to these parameters based on parameter order
                    AssignParameterValues(commandParameters, parameterValues);

                    //call the overload that takes an array of SqlParameters
                    return ExecuteScalar(connectionString, CommandType.StoredProcedure, spName, commandParameters);
                }
                //otherwise we can just call the SP without params
                else
                {
                    return ExecuteScalar(connectionString, CommandType.StoredProcedure, spName);
                }
            }

            //*********************************************************************
            //
            // Execute a SqlCommand (that returns a 1x1 resultset) against the specified SqlConnection 
            // using the provided parameters.
            // 
            // e.g.:  
            //  int orderCount = (int)ExecuteScalar(conn, CommandType.StoredProcedure, "GetOrderCount", new SqlParameter("@prodid", 24));
            // 
            // param name="connection" a valid SqlConnection 
            // param name="commandType" the CommandType (stored procedure, text, etc.) 
            // param name="commandText" the stored procedure name or T-SQL command 
            // param name="commandParameters" an array of SqlParamters used to execute the command 
            // returns an object containing the value in the 1x1 resultset generated by the command
            //
            //*********************************************************************

            public static object ExecuteScalar(SqlConnection connection, CommandType commandType, string commandText, params SqlParameter[] commandParameters)
            {
                //create a command and prepare it for execution
                SqlCommand cmd = new SqlCommand();
                PrepareCommand(cmd, connection, (SqlTransaction)null, commandType, commandText, commandParameters);

                //execute the command & return the results
                object retval = cmd.ExecuteScalar();

                // detach the SqlParameters from the command object, so they can be used again.
                cmd.Parameters.Clear();
                return retval;

            }
        }

        //*********************************************************************
        //
        // SqlHelperParameterCache provides functions to leverage a static cache of procedure parameters, and the
        // ability to discover parameters for stored procedures at run-time.
        //
        //*********************************************************************
        public sealed class SqlHelperParameterCache
        {
            //*********************************************************************
            //
            // Since this class provides only static methods, make the default constructor private to prevent 
            // instances from being created with "new SqlHelperParameterCache()".
            //
            //*********************************************************************

            private SqlHelperParameterCache() { }

            private static Hashtable paramCache = Hashtable.Synchronized(new Hashtable());

            //*********************************************************************
            //
            // resolve at run time the appropriate set of SqlParameters for a stored procedure
            // 
            // param name="connectionString" a valid connection string for a SqlConnection 
            // param name="spName" the name of the stored procedure 
            // param name="includeReturnValueParameter" whether or not to include their return value parameter 
            //
            //*********************************************************************

            private static SqlParameter[] DiscoverSpParameterSet(string connectionString, string spName, bool includeReturnValueParameter)
            {
                using (SqlConnection cn = new SqlConnection(connectionString))
                using (SqlCommand cmd = new SqlCommand(spName, cn))
                {
                    cn.Open();
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlCommandBuilder.DeriveParameters(cmd);

                    if (!includeReturnValueParameter)
                    {
                        cmd.Parameters.RemoveAt(0);
                    }

                    SqlParameter[] discoveredParameters = new SqlParameter[cmd.Parameters.Count]; ;

                    cmd.Parameters.CopyTo(discoveredParameters, 0);

                    return discoveredParameters;
                    //cn.Close();
                }
            }

            private static SqlParameter[] CloneParameters(SqlParameter[] originalParameters)
            {
                //deep copy of cached SqlParameter array
                SqlParameter[] clonedParameters = new SqlParameter[originalParameters.Length];

                for (int i = 0, j = originalParameters.Length; i < j; i++)
                {
                    clonedParameters[i] = (SqlParameter)((ICloneable)originalParameters[i]).Clone();
                }

                return clonedParameters;
            }

            //*********************************************************************
            //
            // add parameter array to the cache
            //
            // param name="connectionString" a valid connection string for a SqlConnection 
            // param name="commandText" the stored procedure name or T-SQL command 
            // param name="commandParameters" an array of SqlParamters to be cached 
            //
            //*********************************************************************

            public static void CacheParameterSet(string connectionString, string commandText, params SqlParameter[] commandParameters)
            {
                string hashKey = connectionString + ":" + commandText;

                paramCache[hashKey] = commandParameters;
            }

            //*********************************************************************
            //
            // Retrieve a parameter array from the cache
            // 
            // param name="connectionString" a valid connection string for a SqlConnection 
            // param name="commandText" the stored procedure name or T-SQL command 
            // returns an array of SqlParamters
            //
            //*********************************************************************

            public static SqlParameter[] GetCachedParameterSet(string connectionString, string commandText)
            {
                string hashKey = connectionString + ":" + commandText;

                SqlParameter[] cachedParameters = (SqlParameter[])paramCache[hashKey];

                if (cachedParameters == null)
                {
                    return null;
                }
                else
                {
                    return CloneParameters(cachedParameters);
                }
            }

            //*********************************************************************
            //
            // Retrieves the set of SqlParameters appropriate for the stored procedure
            // 
            // This method will query the database for this information, and then store it in a cache for future requests.
            // 
            // param name="connectionString" a valid connection string for a SqlConnection 
            // param name="spName" the name of the stored procedure 
            // returns an array of SqlParameters
            //
            //*********************************************************************

            public static SqlParameter[] GetSpParameterSet(string connectionString, string spName)
            {
                return GetSpParameterSet(connectionString, spName, false);
            }

            //*********************************************************************
            //
            // Retrieves the set of SqlParameters appropriate for the stored procedure
            // 
            // This method will query the database for this information, and then store it in a cache for future requests.
            // 
            // param name="connectionString" a valid connection string for a SqlConnection 
            // param name="spName" the name of the stored procedure 
            // param name="includeReturnValueParameter" a bool value indicating whether the return value parameter should be included in the results 
            // returns an array of SqlParameters
            //
            //*********************************************************************

            public static SqlParameter[] GetSpParameterSet(string connectionString, string spName, bool includeReturnValueParameter)
            {
                string hashKey = connectionString + ":" + spName + (includeReturnValueParameter ? ":include ReturnValue Parameter" : "");

                SqlParameter[] cachedParameters;

                cachedParameters = (SqlParameter[])paramCache[hashKey];

                if (cachedParameters == null)
                {
                    cachedParameters = (SqlParameter[])(paramCache[hashKey] = DiscoverSpParameterSet(connectionString, spName, includeReturnValueParameter));
                }

                return CloneParameters(cachedParameters);
            }
        }

        #endregion
    }
}
